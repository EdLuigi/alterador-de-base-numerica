const $ = document.querySelector.bind(document);

$('#valorEntrada').addEventListener('input', e => {
    try {
        let pontoDisponivel = true;
        e.target.value = e.target.value.replace(/,/g, '.').split('').map(s => {
            if (s === '.')
                if (pontoDisponivel) { pontoDisponivel = false; return '.'; }
                else return '';
            else return s;
        }).join('');

        $('#resposta').innerHTML = conversaoDeBase(e.target.value);
    } catch (e) {
        console.log('erro: ' + e);
        $('#resposta').innerHTML = 'erro no input';
    }
});

const conversaoDeBase = (valor) => {
    if (valor.length === 0) return '';
    let pontoIndex = valor.indexOf('.');

    if (pontoIndex !== -1) {


        return 'erro no input';
    }
    else return valor.split('').
        map((v, i, a) => a[i] = v * Math.pow(getBaseOrigem(), a.length - i - 1))
        .reduce((somatoria, elemento) => somatoria + elemento);
};

const getBaseOrigem = () => ($('input[name="baseRadio0"]:checked').value);
const getBaseDestino = () => ($('input[name="baseRadio1"]:checked').value);